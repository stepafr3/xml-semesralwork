# slouceni do jednoho xml souboru
xmllint --dropdtd --noent --output countries.xml 'merge.xml';
# validace pomoci DTD, pokud uspesna, vypise hlasku "DTD validation success"
xmllint --noout --dtdvalid 'validation/schema.dtd' countries.xml && echo "DTD validation success";
# validace pomoci RelaxNG, pokud uspesna, vypise halsku "countries.xml validates"
xmllint --noout --relaxng 'validation/schema.rng' countries.xml;
# vygenerovani html vystupu pro jednotlive oblasti
java -jar ~/Downloads/SaxonHE9-9-1-6J/saxon9he.jar countries.xml transformation/transformPages.xsl;
# vygenerovani index.html, obsahujici odkazy na jednotlive oblasti
java -jar ~/Downloads/SaxonHE9-9-1-6J/saxon9he.jar countries.xml transformation/transformIndex.xsl;
# vygenerovani pdf souboru
fop -xml countries.xml -xsl transformation/transformFO.xsl -pdf countries.pdf;