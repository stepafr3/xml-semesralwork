<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="A4-portrait"
                                       page-height="29.7cm"
                                       page-width="21.0cm"
                                       margin="2cm">
                    <fo:region-body margin="1.5cm"/>
                    <fo:region-before extent="2cm" />
                    <fo:region-after   extent="2cm"
                                       display-align="after">
                    </fo:region-after>
                </fo:simple-page-master>
            </fo:layout-master-set>

            <!-- First page -->
            <fo:page-sequence master-reference="A4-portrait">
                <fo:flow flow-name="xsl-region-body" space-after="2cm">
                    <fo:block font-size="26pt" font-weight="bold" text-align="center">
                        Countries
                    </fo:block>
                    <fo:block font-size="16pt" space-before="0.5cm" text-align="center">
                        Semestral work BI-XML FIT CVUT 2019-2020
                    </fo:block>
                    <fo:block font-size="12pt" space-before="0.5cm" text-align="center">
                        Autor: Frantisek Stepanek
                    </fo:block>
                </fo:flow>
            </fo:page-sequence>

            <!-- Second page -->
            <fo:page-sequence master-reference="A4-portrait">
                <fo:static-content flow-name="xsl-region-before">
                    <fo:block
                            border-bottom-style="solid"
                            font-weight="bold" text-align="end">
                        Countries, Frantisek Stepanek
                    </fo:block>
                </fo:static-content>
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block text-align="center">
                        <fo:page-number/>
                    </fo:block>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body" space-after="2cm">
                    <fo:block font-size="20pt" space-before="0.5cm" font-weight="bold" text-decoration="underline" page-break-before="always">
                        Content:
                    </fo:block>
                    <xsl:apply-templates mode="content"/>
                </fo:flow>
            </fo:page-sequence>


            <!--Countries -->
            <fo:page-sequence master-reference="A4-portrait">
                <fo:static-content flow-name="xsl-region-before">
                    <fo:block
                            border-bottom-style="solid"
                            font-weight="bold" text-align="end">
                        Countries, Frantisek Stepanek
                    </fo:block>
                </fo:static-content>
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block text-align="center">
                        <fo:page-number/>
                    </fo:block>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body" space-after="2cm">
                    <xsl:apply-templates mode="pages"/>
                </fo:flow>
            </fo:page-sequence>

        </fo:root>
    </xsl:template>

    <xsl:template match="/*/country" mode="content">
        <fo:block font-size="14pt" font-weight="bold" space-before="0.5cm">
            <fo:basic-link internal-destination="{@name}"
                           text-decoration="underline">
                <xsl:value-of select="@name"/>
            </fo:basic-link>
        </fo:block>
    </xsl:template>

    <xsl:template match="/*/country" mode="pages">
        <fo:block font-size="22pt" font-weight="bold" id="{@name}" page-break-before="always">
            <xsl:value-of select="@name"/>
        </fo:block>
        <fo:block font-size="22pt" font-weight="bold" >
            <xsl:apply-templates mode="content"/>
        </fo:block>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="/*/*/*[@name]" mode="content">
        <fo:block font-size="14pt" font-weight="bold" space-before="0.5cm">
            <fo:basic-link internal-destination="{../@name}-{@name}"
                           text-decoration="underline">
                <xsl:value-of select="@name"/>
            </fo:basic-link>
        </fo:block>
    </xsl:template>

    <xsl:template match="/*/*/*[@src]">
        <fo:block font-size="22pt" font-weight="bold" id="{../@name}-{@name}" text-decoration="underline" page-break-before="always">
            <xsl:value-of select="@name"/>
        </fo:block>
        <fo:block>
            <fo:external-graphic src="url('data/{@src}')" content-height="32em" content-width="32em"/>
        </fo:block>
    </xsl:template>

    <xsl:template match="/*/*/*[not(@src)]">
        <fo:block font-size="22pt" font-weight="bold" id="{../@name}-{@name}" text-decoration="underline" page-break-before="always">
            <xsl:value-of select="@name"/>
        </fo:block>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="/*/*/*/*[@name]">
        <fo:block font-size="14pt" font-weight="bold"  space-before="0.5cm" keep-with-next="always">
            <xsl:value-of select="@name"/>
        </fo:block>
        <fo:block font-size="11pt">
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="/*/*/*/*/*">
        <fo:block text-align="justify">
            <xsl:value-of select="."/>
        </fo:block>
    </xsl:template>
</xsl:stylesheet>