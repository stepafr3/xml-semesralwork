<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:output omit-xml-declaration='yes' indent='yes'/>

    <xsl:template match="/">
        <xsl:apply-templates mode="pages"/>
    </xsl:template>


    <xsl:template match="//country" mode="pages">
        <xsl:result-document href="html/{@name}.html">
            <html>
                <head>
                    <title>
                        Countries
                    </title>
                    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
                </head>
                <body>
                    <header>
                        <h1>Countries</h1>
                        <a href="index.html">Main page</a>
                    </header>
                    <main>
                        <div class="country">
                            <h2>
                                <xsl:value-of select="@name"/>
                                <hr/>
                            </h2>
                        </div>
                        <div class="main-section">
                                 <h3>Content</h3>
                                <hr/>
                                 <pp>
                                     <xsl:apply-templates mode="menu"/>
                                 </pp>
                            <xsl:apply-templates/>
                        </div>
                    </main>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="/*/*/*[@name]" mode="menu">
        <li>
            <a href="#{@name}">
                <xsl:value-of select="@name"/>
            </a>
        </li>
    </xsl:template>

    <xsl:template match="/*/*/*[@src]">
        <div class="main-section">
            <h3 id="{@name}">
                <xsl:value-of select="@name"/>
            </h3>
            <hr/>
            <img src="../data/{@src}"/>
        </div>
    </xsl:template>

    <xsl:template match="/*/*/*[not(@src)]">
        <div class="main-section">
            <h3 id="{@name}">
                <xsl:value-of select="@name"/>
            </h3>
            <hr/>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="/*/*/*/*">
        <div class="sub-section">
            <h4>
                <xsl:value-of select="@name"/>
            </h4>
            <hr/>
            <p>
                <xsl:apply-templates/>
            </p>
        </div>
    </xsl:template>

    <xsl:template match="/*/*/*/*/*">
        <p>
            <xsl:value-of select="."/>
        </p>
    </xsl:template>

</xsl:stylesheet>