# Semestrální práce BI-XML

* Semestrální práce na předmět BI-XML na FIT ČVUT 2019-2020
* Autor:  František Štěpánek

## Potřebné software
* xmllint
* fop

## Adresářová struktura

 * data
    * Obsahuje:
        * xml soubory pro jednotlivé oblasti
        * Oblasti:
            * Finland, Greece, Solomon Islands, Uruguay
        * obrázky vlajek a map pro jednotlivé oblasti
        * Zdroj: https://www.cia.gov/library/publications/resources/the-world-factbook/
        * Poznámka: XML soubory jsem vytvářel ručně, proto neobsahují všechny informace z uvedeného zdroje
* html
    * Obsahuje:
        * vygenerované HTML výstupy pro jednotlivé stránky
        * index.html, obsahující odkazy na jednotlivé stránky
        * main.css, definující výsledný vzhled stránek
* transformation
    * Obsahuje:
        * transformIndex.xsl, pomocí kterého je vygenerován soubor index.html
        * trasnformPages.xsl, pomocí kterého jsou vygenerovány html soubory pro jednotlivé státy
        * transformFO.xsl, pomocí kterého je vygenerován soubor countries.pdf
* validation
    * Obsahuje:
        * schema.dtd, který validuje soubor countries.xml pomocí DTD
        * schema.rng, který validuje soubor countris.xml pomocí RelaxNG
* další soubory:
    * merge.xml, sloučí jednotlivé xml soubory do jednoho s názvem coutries.xml
    * all.sh, script, obsahující všechny příkazy, pro sestavení práce, validace a vygenerování pdf výstupu
    * projekt_specification_stepanek.xml, specifikace zadání
## Postup pro sestavení

### Sloučení do jednoho XML souboru
    xmllint --dropdtd --noent --output countries.xml 'merge.xml'

### Validace
#### DTD
    xmllint --noout --dtdvalid 'validation/schema.dtd' countries.xml

#### RelaxNG
    xmllint --noout --relaxng 'validation/schema.rng' countries.xml
    
    
### Vygenerování HTML
Poznámka: cesta k saxon9he.jar se může na různých zařízeních lišit
 #### Vygenerování index.html stránky
    java -jar ~/Downloads/SaxonHE9-9-1-6J/saxon9he.jar countries.xml transformation/transformIndex.xsl 
 #### Vygenerování stranky pro každou zemi
    java -jar ~/Downloads/SaxonHE9-9-1-6J/saxon9he.jar countries.xml transformation/transformPages.xsl

### Vygenerování PDF
    fop -xml countries.xml -xsl transformation/transformFO.xsl -pdf countries.pdf
    
### Spuštění all.sh
    chmod +x all.sh
    ./all.sh